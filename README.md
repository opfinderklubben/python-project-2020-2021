# python-project-2020-2021

A Python project in Opfinderklubben. We are experimenting with
game programming in Python using the [Arcade library](https://arcade.academy).

A [talk](https://www.youtube.com/watch?v=DAWHMHMPVHU) on how to create games with Python Arcade.

A [talk](https://www.youtube.com/watch?v=2SMkk63k6Ik) on multiplayer games with sockets and Python Arcade.

# How to run the game

Run the game client in _src/client_: `python3 client.py`

# Project description

* Uses Python Arcade library.
* A top down 2D world of square tiles (Bigger than user's screen).
* Players can move around in the world. Camera follows player.
* Worlds designed in [Tiled](https://www.mapeditor.org), and imported.
* Graphics are one of two themes
  1. [Pirates](https://kenney.nl/assets/pirate-pack)
  2. [Tanks](https://kenney.nl/assets/topdown-tanks-redux)
* Multiplayer using [websockets](https://websockets.readthedocs.io/en/stable/)
  1. A Python client
  2. A Python server
* Server holds game state. Clients are only screens and collectors of user inputs (Mouse, keys).
* Players constantly send their input's state to the server
* Server sends resulting game state to players

# To do
A birds level list of things which need to be done 
## Client/server architecture
- [ ] [Listen on a websockets](https://websockets.readthedocs.io/en/stable/intro.html#basic-example)
- [ ] [Send data to websocket](https://websockets.readthedocs.io/en/stable/intro.html#basic-example)
- [ ] [Subscribe to data via websocket](https://websockets.readthedocs.io/en/stable/intro.html#synchronization-example)

# Client
- [x] Load a map produced by Tiled in a client based on this [demo](https://arcade.academy/examples/sprite_tiled_map_with_levels.html)
- [ ] Move camera/player.

# Levels

Levels must contain the following layers:

1. _water_
2. _shallows_
3. _land_
4. _environment_
5. _buildings_

- [x] Design a proof-of-concept level of size 32x32 tiles.
- [ ] Design an interesting level of size 64x64 tiles.

# Game design
- [ ] A simple idea for game rules.
