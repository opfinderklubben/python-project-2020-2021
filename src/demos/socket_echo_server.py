#!/usr/bin/env python3

import asyncio
import websockets

port = 8765

async def echo(websocket, path):
    async for message in websocket:
        print(f"Recieved message: {message}")
        await websocket.send(message)

start_server = websockets.serve(echo, "localhost", port)

print(f"Listening for connections on port {port}. CTRL+C to quit.")
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
