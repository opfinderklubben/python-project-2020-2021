#!/usr/bin/env python3

import asyncio
import websockets

port = 8765

message = "Hello world!"

async def hello():
    uri = f"ws://localhost:{port}"
    async with websockets.connect(uri) as websocket:
        await websocket.send(message)
        answer = await websocket.recv()
        print(f"Server sent me: {answer}")

asyncio.get_event_loop().run_until_complete(hello())
